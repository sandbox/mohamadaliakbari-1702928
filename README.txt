Module: Commerce MellatAuthor: Mohammad Ali Akbari http://rastasoft.ir
Description===========Adds a payment method to Drupal Commerce to accept credit card payments throughthe Mellat (Iranian bank) web service.Only works with Rials (IRR) currency.
Requirements============* Commerce* Commerce Payment
Installation============* Place Commerce Mellat module into your Drupal sites/all/modules directory as  usual.* Download nusoap from http://sourceforge.net/projects/nusoap/ and extract in  libraries directory. so you should have /sites/all/libraries/nusoap/nusoap.php* Enable the module from the Modules > COMMERCE (CONTRIB) section
Setup=====* Go to Commerce Admin Menu > Payment Methods and enable Mellat Bank Gateway.* Edit Mellat Bank Gateway and enter Terminal Id, Username and Password.
History=======First beta1 release: 27/7/2012